import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { Todo } from './models/todo';

@Injectable({
	providedIn: 'root',
})
export class TodoService {
	private observable?: Observable<Todo[]>;
	private subscriber?: Subscriber<Todo[]>;

	constructor() {}

	isEditing: Todo | null = null;

	private localStorageGet(): Todo[] {
		const todoData = localStorage.getItem('todos');
		const todos: Todo[] = todoData ? JSON.parse(todoData) : [];
		return todos.sort((a, b) =>
			a.complete === b.complete ? 0 : a.complete && !b.complete ? 1 : -1
		);
	}

	private localStorageSet(todos: Todo[]) {
		localStorage.setItem('todos', JSON.stringify(todos));
	}

	setEditing(todo: Todo) {
		this.isEditing = todo;
	}

	releaseEditing() {
		this.isEditing = null;
	}

	add(todo: Todo) {
		todo.id = uuidv4();
		this.localStorageSet([todo, ...this.localStorageGet()]);
		this.subscriber?.next(this.localStorageGet());
	}

	update(todo: Todo) {
		const todos = this.localStorageGet();
		const index = todos.findIndex((t) => t.id === todo.id);
		todos.splice(index, 1, todo);
		this.localStorageSet(todos);
		this.subscriber?.next(this.localStorageGet());
	}

	delete(todo: Todo) {
		this.localStorageSet(
			this.localStorageGet().filter((t) => t.id !== todo.id)
		);
		this.subscriber?.next(this.localStorageGet());
	}

	get() {
		return (
			this.observable ||
			(this.observable = new Observable<Todo[]>((subscriber) => {
				subscriber.next(this.localStorageGet());
				this.subscriber = subscriber;
			}))
		);
	}
}
