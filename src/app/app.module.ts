import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppComponent } from './app.component';
import { ContenteditableDirective } from './helpers/contenteditable.directive';
import { TodoItemComponent } from './todo-item/todo-item.component';

@NgModule({
	declarations: [AppComponent, TodoItemComponent, ContenteditableDirective],
	imports: [BrowserModule, FontAwesomeModule, ReactiveFormsModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
