import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { Todo } from './models/todo';
import { TodoService } from './todo.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	faPlus = faPlus;

	todos?: Observable<Todo[]>;

	constructor(private todoService: TodoService) {}

	get newTodo() {
		return this.todoService.isEditing?.id
			? undefined
			: this.todoService.isEditing;
	}

	addTodo() {
		this.todoService.setEditing(
			new Todo({
				title: 'New Todo',
				description: 'description',
			})
		);
	}

	ngOnInit(): void {
		this.todos = this.todoService.get();
	}

	trackBy(index: number, todo: Todo) {
		return todo.id;
	}
}
