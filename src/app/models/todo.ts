export class Todo {
	id?: string;
	title!: string;
	description?: string;
	complete: boolean = false;

	constructor(data?: Partial<Todo>) {
		if (data) {
			Object.assign(this, data);
		}
	}
}
