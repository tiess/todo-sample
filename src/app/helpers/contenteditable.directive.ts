import { DOCUMENT } from '@angular/common';
import {
	Attribute,
	Directive,
	ElementRef,
	forwardRef,
	HostListener,
	Inject,
	Renderer2,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
	selector: '[contenteditable],[contentEditable]',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => ContenteditableDirective),
			multi: true,
		},
	],
})
export class ContenteditableDirective implements ControlValueAccessor {
	private onChange?: (value: string) => void;
	private onTouched?: () => void;

	constructor(
		private elementRef: ElementRef,
		private renderer: Renderer2,
		@Attribute('unformattedPaste') private unformattedPaste: string,
		@Inject(DOCUMENT) private document: Document
	) {}

	writeValue(value: any): void {
		this.renderer.setProperty(
			this.elementRef.nativeElement,
			'textContent',
			value
		);
	}

	registerOnChange(fn: () => void): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: () => void): void {
		this.onTouched = fn;
	}

	@HostListener('input')
	_onChange() {
		this.onChange?.(this.elementRef.nativeElement.textContent);
	}

	@HostListener('blur')
	_onTouched() {
		this.onTouched?.();
	}
}
