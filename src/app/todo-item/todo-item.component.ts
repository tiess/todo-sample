import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {
	faCheck,
	faPencil,
	faTrash,
	faXmark,
} from '@fortawesome/free-solid-svg-icons';
import { Todo } from '../models/todo';
import { TodoService } from '../todo.service';

@Component({
	selector: 'app-todo-item[todo]',
	templateUrl: './todo-item.component.html',
	styleUrls: ['./todo-item.component.scss'],
})
export class TodoItemComponent implements OnInit {
	faTrash = faTrash;
	faPencil = faPencil;
	faCheck = faCheck;
	faXmark = faXmark;

	@Input() todo!: Todo;

	form = this.fb.group({
		title: ['', Validators.required],
		description: '',
	});

	constructor(private todoService: TodoService, private fb: FormBuilder) {}

	ngOnInit(): void {
		this.form.patchValue(this.todo);
	}

	deleteTodo() {
		this.todoService.delete(this.todo);
	}

	edit() {
		this.todoService.isEditing = this.todo;
	}

	update() {
		console.log('update', this.form.value);
		Object.assign(this.todo, this.form.value);
		if (this.todo.id) {
			this.todoService.update(this.todo);
		} else {
			this.todoService.add(this.todo);
		}
		this.form.reset(this.todo);
		this.todoService.isEditing = null;
	}

	toggleComplete() {
		this.todo.complete = !this.todo.complete;
		this.todoService.update(this.todo);
	}

	cancel() {
		this.form.reset(this.todo);
		this.todoService.isEditing = null;
	}

	get isEditingAnything() {
		return this.todoService.isEditing !== null;
	}

	get isEditing() {
		return this.todoService.isEditing === this.todo;
	}
}
